#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.ws;

import javax.jws.WebService;

/* Update the service name used in the endpoint URI */
@WebService(serviceName="service")
public interface CXFService {

	public Object serviceOperationMethod();
	
}
