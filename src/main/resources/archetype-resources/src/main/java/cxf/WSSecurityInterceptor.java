#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.cxf;

import javax.security.auth.Subject;

import org.apache.cxf.common.security.SecurityToken;
import org.apache.cxf.common.security.TokenType;
import org.apache.cxf.common.security.UsernameToken;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.security.AbstractUsernameTokenInInterceptor;
import org.apache.cxf.interceptor.security.AuthenticationException;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WSSecurityInterceptor extends AbstractUsernameTokenInInterceptor {

	private final static Logger logger = LoggerFactory.getLogger(WSSecurityInterceptor.class);

	private String verificationUsername;

	private String verificationPassword;

	private boolean reportFault = false;

	@Override
	public void handleMessage(Message message) throws Fault {

		String name = null;
		String password = null;
		SecurityToken token = null;

		AuthorizationPolicy policy = message.get(AuthorizationPolicy.class);
		if (policy != null) {
			name = policy.getUserName();
			password = policy.getPassword();
		} else {
			// try the UsernameToken
			token = message.get(SecurityToken.class);
			if (token != null && token.getTokenType() == TokenType.UsernameToken) {
				UsernameToken utoken = (UsernameToken) token;
				name = utoken.getName();
				password = utoken.getPassword();
			}
		}

		if (name == null || password == null) {
			throw new AuthenticationException("Authentication required but no user or password was supplied");
		}

		try {
			
			if (!(verificationUsername.equals(name)) || !(verificationPassword.equals(password)) ) {
				throw new Exception("Authentication required please verify credentials and try again!");
			}

		} catch (Exception ex) {
			String errorMessage = "Authentication failed for user " + name + " : " + ex.getMessage();

			if (logger.isErrorEnabled()) {
				logger.error(errorMessage, ex);
			}

			if (reportFault) {
				throw new AuthenticationException(errorMessage);
			} else {
				throw new AuthenticationException("Authentication failed (details can be found in server log)");
			}
		}
	}

	public String getVerificationUsername() {
		return verificationUsername;
	}

	public void setVerificationUsername(String verificationUsername) {
		this.verificationUsername = verificationUsername;
	}

	public String getVerificationPassword() {
		return verificationPassword;
	}

	public void setVerificationPassword(String verificationPassword) {
		this.verificationPassword = verificationPassword;
	}

	public boolean isReportFault() {
		return reportFault;
	}

	public void setReportFault(boolean reportFault) {
		this.reportFault = reportFault;
	}

	@Override
	protected Subject createSubject(UsernameToken token) {
		return createSubject((SecurityToken)token);
	}

}
